<?php

namespace system;

use Exception;

class View
{

    static $blocks = array();
    static $cache =  VIEW_CACHE;
    static $view_path = 'views';
    static $errors = array();
    /**
     * Renderiza y guarda en cache la vista para realizar un include del objeto
     * dentro de un sreamin de memoria
     * @param $file ruta de la vista
     */
    static function render($file)
    {
        $code = self::buildView($file);
        set_error_handler(function (int $number, string $message) {
            echo "Error $number: '$message'" . PHP_EOL;
        });


        try {
            ob_start();
            //Obtiene el segundo argumento si en caso existe
            if (isset(func_get_args()[1])) {
                foreach (func_get_args()[1] as $key => $value) {
                    ${$key} = $value;
                }
                extract(func_get_args()[1], EXTR_SKIP);
            }
            eval('?>' . $code . '<?');
            ob_flush();
            ob_end_clean(); // Clear the buffer.
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage() . PHP_EOL;
        }
    }

    static function buildView($file)
    {
        $file_ = CACHE_PATH . "/" . str_replace("/", "_", $file);

        if (VIEW_CACHE && file_exists($file_)) {
            return $file_;
        }
        $file = VIEW_PATH . "/$file";
        $code = file_get_contents($file);
        $code = self::getBlocks($code);
        $code = self::extendsFiles($code);
        $code = self::replaceVars($code);
        return $code;
    }
    static function replaceVars($code)
    {
        $code = str_replace("{{f_static}}", URL_BASE . STATIC_FILES_PATH, $code);
        $code = str_replace(["{{link}}/", "{{link}}"], URL_BASE, $code);
        return $code;
    }

    static function extendsFiles($code)
    {
        $n = 0;
        $template = "";
        $pattern = "/{% ?extends (.*) ?%}/i";
        preg_match_all($pattern, $code, $files, PREG_SET_ORDER);
        foreach ($files as $file) {
            $name_file = trim($file[1]);
            $template = file_get_contents(VIEW_PATH . "/$name_file.php");
            $code = str_replace($file[0], "", $code);
            $n++;
        }
        if ($n > 1) {
            static::$errors['extends'] = "No puede heredar mas de un template";
        }
        if ($n == 1) {
            $pattern = "/{% ?block ?(.*?) ?%}/is";
            preg_match_all($pattern, $template, $blocks, PREG_SET_ORDER);
            foreach ($blocks as $block) {
                if (array_key_exists($block[1], self::$blocks) == true) {
                    $template = str_replace($block[0], self::$blocks[$block[1]], $template);
                } else {
                    $template = str_replace($block[0], "", $template);
                }
            }
            return $template;
        } else {
            foreach (self::$blocks as $block) {
                $code .= $block;
            }
            return $code;
        }
    }

    /**
     * Obtiene todos los bloques de la vista
     * 
     * @param $code Código fuente vista
     * @return $code
     */
    static function getBlocks($code)
    {
        $pattern = "/{% ?block ?(.*?) ?%}(.*?){% ?endblock ?%}/is";
        preg_match_all($pattern, $code, $blocks, PREG_SET_ORDER);
        foreach ($blocks as $block) {
            $code = str_replace($block[0], "", $code);
            self::$blocks[$block[1]] = $block[2];
        }
        return $code;
    }
}
