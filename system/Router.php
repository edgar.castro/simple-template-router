<?php

namespace system;

class Router
{
    private static $args = array();
    static $errors = array();
    static $routes = array();
    private static $route = "";
    private static $runed = false;

    static function get($route, $callback = false)
    {
        if (static::addRoute($route, 'GET'))
            static::run($callback);
    }
    static function post($route, $callback = false)
    {
        if (static::addRoute($route, 'POST'))
            static::run($callback);
    }
    static function put($route, $callback = false)
    {
        if (static::addRoute($route, 'PUT'))
            static::run($callback);
    }
    static function delete($route, $callback = false)
    {
        if (static::addRoute($route, 'DELETE'))
            static::run($callback);
    }

    static function addRoute($route, $method)
    {
        if (!static::validateMethod($method))
            return false;
        static::$route = $route;
        $add = true;
        foreach (static::$routes as $key => $value) {
            if ($method == $value['method'] && $route == $value['route']) {
                static::$errors[] = "Route duplicated : $route";
                $add = false;
                break;
            }
        }
        $add ? static::$routes[] = array("method" => $method, "route" => $route) : null;
        return true;
    }

    static function routeController($controller)
    {
    }
    static function run($callback)
    {
        if (static::$runed)
            return;

        if (!static::matchUri(static::$route))
            return;
        if ($callback) {
            static::$runed = true;
            $callback(...array_values(static::$args));
        }
    }
    static function matchUri($uri)
    {
        static::$args = array();
        $uri_ = explode("?", str_replace(URL_BASE, "/", $_SERVER['REQUEST_URI']))[0];

        $uri = preg_replace('/\/*\z/', '', $uri);
        $uri_ = preg_replace('/\/*\z/', '', $uri_);
        if ($uri == $uri_) {
            static::$runed = true;
            return true;
        }

        $part = explode('/', $uri);
        $part_ = explode('/', $uri_);
        if (count($part) != count($part_)) {
            return false;
        }

        foreach ($part as $key => $value) {
            preg_match_all('/\[(?<type>\w+)=(?<name>\w+)\]/', $value, $match, PREG_SET_ORDER);

            if (count($match) > 0) {
                if (static::validateParam($match[0], $part_[$key])) {
                    static::$args[$match[0]['name']] = $part_[$key];
                } else {
                    return false;
                }
            } else {
                if ($value != $part_[$key]) {
                    return false;
                }
            }
        }

        return true;
    }

    static function validateParam($type_name, $param)
    {
        $resp = false;
        switch ($type_name['type']) {
            case 'number':
                $resp = ctype_digit($param);
                break;

            case 'float':
                $resp = ctype_alpha($param) ? false : filter_var($param, FILTER_VALIDATE_FLOAT);
                break;

            case 'string':
                $resp = ctype_alnum($param);
                break;

            default:

                break;
        }
        return $resp;
    }

    static function validateMethod($method)
    {
        return $_SERVER['REQUEST_METHOD'] == $method ? true : false;
    }
    static function getRequest()
    {
        $request = array();
        if (count($_REQUEST)) {
            foreach ($_REQUEST as $key => $value) {
                $request[$key] = $value;
            }
        }
        $data = json_decode(file_get_contents("php://input"), true);

        if (is_array($data) && count($data) > 0) {
            foreach ($data as $key => $value) {
                $request[$key] = $value;
            }
        }
        return $request;
    }

    static function routeError($nro, $callback = false)
    {
        switch ($nro) {
            case 400:
                echo "Error 400";
                break;
            case 401:
                echo "Error 401";
                break;
            case 402:
                echo "Error 402";
                break;
            case 404:
                if (!static::$runed) {
                    if ($callback)
                        $callback();
                    else
                        echo "Page not found";
                }
                break;
            case 500:
                echo "Error 500";
                break;

            default:
                break;
        }
    }
}
