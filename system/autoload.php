<?php
function autoload($class) {
    $file= str_replace("\\","/",$class).".php";
    require_once "$file";
}

spl_autoload_register("autoload");