<?php

namespace config;

define("CONF_DEV", 1);
define("CONF_PROD", 2);
define("URL_BASE", "/web_app/");
define("CACHE_PATH", "cache");
define("VIEW_PATH", "views");
define("STATIC_FILES_PATH", "static");
define("VIEW_CACHE", false);

class Config
{
    static function setConf($conf)
    {
        switch ($conf) {
            case CONF_DEV:
                $conf = static::getDev();
                break;
            case CONF_PROD:
                $conf = static::getProd();
                break;

            default:
                $conf = static::getDev();
                break;
        }
        foreach ($conf as $key => $value) {
            define($key, $value);
        }
    }

    static function getDev()
    {
        return [
            "DB_IP" =>  "localhost",
            "DB_PORT" => "3366",
            "DB_NAME" => "crud",
            "DB_USER" =>  "root",
            "DB_PASS" =>  "pass_dev"
        ];
    }
    static function getProd()
    {
        return [
            "DB_IP" =>  "localhost",
            "DB_PORT" => "3366",
            "DB_NAME" => "crud",
            "DB_USER" =>  "root",
            "DB_PASS" =>  "pass_prod"
        ];
    }
}
