<?php
require_once "./system/autoload.php";
use config\Config;
use controller\UserController;
use models\User;
use controller\extra\Utility;
use system\Router;
use system\View;

Config::setConf(CONF_DEV);

Router::get('/',function(){
    View::render('index.php',['casa'=>'cualquiera1']);
});
Router::get('/home',function(){
    View::render('index.php',['casa'=>'cualquiera1']);
});
Router::get('/page_1/',function(){
    $controller = new UserController();
    $controller->getUserByName(false);
});
Router::get('/page_1/[string=name]/',function($name){
    $controller = new UserController();
    $controller->getUserByName($name);
});

Router::routeError(404,function(){
    View::render("error/404.php");
});
