{% extends layout/base %}

{% block title %} Usuario {% endblock %}

{% block navtitle %}
<label>/Usuario/Detalle</label>
{% endblock %}

{% block header %}
<h3 class="text-center"> Detalle Usuario</h3>
{% endblock %}

{% block content %}
<?php
if (isset($user)) {
    echo "<p><label>Nombre :</label><label> $user->name </label></p>";
    echo "<p><label>Apellido : </label><label> $user->surname </label></p>";
}
?>
{% endblock %}

{% block footer %}
<p>  </p>
{% endblock %}