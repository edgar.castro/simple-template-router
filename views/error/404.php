{% extends layout/error %}

{% block title %} 404 {% endblock %}


{% block header %}
    <h1 class="err">404</h1>
{% endblock %}

{% block content %}
    <h1 class="msg">Page not found</h1>
{% endblock %}

{% block footer %}
    ..
{% endblock %}