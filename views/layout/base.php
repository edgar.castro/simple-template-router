<!DOCTYPE html>
<html>

<head>
    <title>{% block title %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{f_static}}/css/site.css" />
    <script type="text/javascript" src="{{f_static}}/js/site.js"></script>
    <style>

    </style>
</head>

<body>
    <header>
        <div class="logo">
            <a href="{{link}}/home">
                <picture>
                    <!-- <source srcset="static/img/logo.png" media="(min-width: 600px)"> -->
                    <img src="{{f_static}}/img/logo.png" alt="">
                </picture>
            </a>
        </div>
        <nav class="top-menu">
            <ul>
                <li><a href="{{link}}/home"> Home</a></li>
                <li><a href="{{link}}/page_1"> Usuarios</a></li>
            </ul>
        </nav>
    </header>
    <main class="content">
        <aside id="main-menu">
            <div id="btn-menu">&nbsp;
            </div>
            <nav class="menu">
                {% block navtitle %}
                <ul>
                    <li><a href="{{link}}/home"> Home</a></li>
                    <li><a href="{{link}}/page_1"> Usuarios</a></li>
                </ul>
            </nav>
        </aside>
        <section>
            {% block header %}
            {% block content %}
            {% block footer %}
        </section>
    </main>
    <footer>
        <p class="t-right">
            ©Edgar Castro (2021) <br>
            <label type="mail">edgar.castro.vm@gmail.com </label>
        </p>
    </footer>
</body>

</html>