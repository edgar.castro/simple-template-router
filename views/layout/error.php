<!DOCTYPE html>
<html>

<head>
    <title>{% block title %}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{f_static}}/css/site.css" />
    <link rel="stylesheet" type="text/css" href="{{f_static}}/css/error.css" />
    <script type="text/javascript" src="{{f_static}}/js/site.js"></script>
    <style>

    </style>
</head>

<body>
    <header>
        <div class="logo">
            <a href="{{link}}/home">
                <picture>
                    <!-- <source srcset="static/img/logo.png" media="(min-width: 600px)"> -->
                    <img src="{{f_static}}/img/logo.png" alt="">
                </picture>
            </a>
        </div>
    </header>
    <main class="content t-center">
        <div class=" ">
            {% block header %}
            {% block content %}
        </div>
    </main>
    <footer>
        {% block footer %}
    </footer>
</body>

</html>