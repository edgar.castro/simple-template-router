# ___SIMPLE TEMPLATE AND ROUTER___

_El proyecto contiene un simple template manager y simple router que te permitirá crear proyectos básicos en PHP._
_El proyecto tiene fines didácticos, cualquier mejora que pueda ser puesta en el código fuente es bienvenida._

---
### Pre-requisitos 
Tener instalado 
php >= 7.4
apache>= 2.4
El módulo rewrite de apache de estar habilitado.

---
### Instalaci&oacute;n
Clonamos el repositorio
```
git clone git@gitlab.com:edgar.castro/simple-template-router.git /var/www/path_to_app
sudo chown -R www-data:www-data /var/www/path_to_app
sudo chmod -R 775 /var/www/path_to_app

```
---
###  Mapa del sistema

|/path_to_app/ |      Directorio raíz                                   |
|--------------|---------------------------------------------------------|
|	config/                             |Almacena los archivos de configuración
|   &emsp;&emsp;Config.php
|	controller/                         |Almacena los controladores de las vistas
|   &emsp;&emsp;extra/                  |Almacena clases utilitarias
|   &emsp;&emsp;&emsp;&emsp;Utility.php
|   &emsp;&emsp;UserController.php
|	models/                             |Almacena los modelos de la tablas
|   &emsp;&emsp;User.php
|	static/                             |Almacena los archivos estáticos css, js, imágenes,|etc
|   &emsp;&emsp;js/
|   &emsp;&emsp;&emsp;&emsp;site.js
|   &emsp;&emsp;img/
|   &emsp;&emsp;&emsp;&emsp;logo.png
|   &emsp;&emsp;css/
|   &emsp;&emsp;&emsp;&emsp;site.css
|   &emsp;&emsp;&emsp;&emsp;error.css
|	system/                             |Almacena los archivos esenciales del sistema
|   &emsp;&emsp;View.php                |Manejador de templates
|   &emsp;&emsp;Router.php              |Router
|   &emsp;&emsp;autoload.php            |carga automática de archivos php
|	views/                              |Almacena las vistas de nuestra pagina
|   &emsp;&emsp;index.php
|   &emsp;&emsp;layout/                 |Almacena los templates
|   &emsp;&emsp;&emsp;&emsp;error.php
|   &emsp;&emsp;&emsp;&emsp;base.php
|   &emsp;&emsp;error/                  |Almacena las vistas de error
|   &emsp;&emsp;&emsp;&emsp;404.php
|	.htaccess                           |Reglas reescritura y redirección
|	index.php                           |Archivo que recibe todas la peticiones (aquí se rutean las vistas)
|	README.md                           |Archivo README
---

### Configuraci&oacute;n
Editar el archivo **config/Config.php** en **URL_BASE** poner el path donde se instal&oacute; el app **(/)** para raíz o **(/path_to_app/)**

```
define("URL_BASE", "/path_to_app/");
```

Acceso a base de datos:
Tenemos dos funciones getDev y getProd en cada una de ellas se debe configurar poner las credenciales del servidor que corresponda.

_Nota_: Actualmente el sistema no cuenta con un acceso a base de datos esto queda a elección del desarrollador
```
   static function getDev()
   {
       return [
           "DB_IP" =>  "localhost",
           "DB_PORT" => "3366",
           "DB_NAME" => "crud",
           "DB_USER" =>  "root",
           "DB_PASS" =>  "pass_dev"
       ];
   }
   static function getProd()
   {
       return [
           "DB_IP" =>  "localhost",
           "DB_PORT" => "3366",
           "DB_NAME" => "crud",
           "DB_USER" =>  "root",
           "DB_PASS" =>  "pass_prod"
       ];
   }
```
Setear la configuración que usará el sistema
Editamos el archivo ***/path_to_app/index.php***
_Ambiente desarrollo_
```
Config::setConf(CONF_DEV);
```
_Ambiente producción_
```
Config::setConf(CONF_PROD);
```
---
### Despliegue

Ingresamos la ruta de nuestra app en el navegador de nuestra preferencia

```
localhost://path_to_app/
```
---
Edgar Castro © 2021 
edgar.castro.vm@gmail.com