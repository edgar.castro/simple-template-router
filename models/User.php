<?php

namespace models;

class User
{
    public $name;
    public $surname;
    
    public function __construct($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
    public function __invoke($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
}
