document.addEventListener("DOMContentLoaded", function (event) {
    let menu = document.getElementById("main-menu")
    let btn_menu = document.getElementById("btn-menu")
    btn_menu.addEventListener("click", () => {
        menu.classList.toggle("show-menu")
    })
})