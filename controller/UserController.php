<?php

namespace controller;

use system\View;
use models\User;

class UserController
{
    private $lstUser;

    function __construct()
    {
        $this->lstUser = [
            new User("Jaime", "Romero"),
            new User("Juan", "Cáceres"),
            new User("Raul", "Romero"),
            new User("Maria",  "Solis"),
            new User("Lucia",  "Del Carmen"),
            new User("Brenda",  "Marin"),
            new User("Miguel", "Cáceres")
        ];
    }

    public function getUserByName($name)
    {
        $user = false;
        if ($name) {
            foreach ($this->lstUser as $u) {
                if ($u->name === $name) {
                    $user = $u;
                    break;
                }
            }
            View::render('page_2.php', ['user' => $user]);
        } else {
            View::render('page_1.php', ['users' => $this->lstUser]);
        }
    }
}
